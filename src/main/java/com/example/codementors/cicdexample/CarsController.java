package com.example.codementors.cicdexample;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class CarsController {

    @GetMapping
    public String getCars() {
        return "Toyota Corolla, VW Golf";
    }
}
